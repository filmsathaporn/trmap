const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://postgres:P@55word@localhost:5432/trh';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query(
  'CREATE TABLE map_data(id SERIAL PRIMARY KEY, location TEXT not null, complete BOOLEAN)');
query.on('end', () => { client.end(); });